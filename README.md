# 多开神器

#### 项目介绍
多开一些只允许运行单个实例的程序，比如微信

![多开神器主界面](https://images.gitee.com/uploads/images/2018/1214/173002_1e7efd89_1538448.jpeg "main.jpg")

![多开微信效果](https://images.gitee.com/uploads/images/2018/1214/173028_1c76e159_1538448.jpeg "result.jpg")

详细介绍及使用说明：[https://8lhx.com/pc-software/multi_start/](https://8lhx.com/pc-software/multi_start/)

#### 更新&下载
项目发布页 [https://gitee.com/leftshine/multi_start/releases](https://gitee.com/leftshine/multi_start/releases)
