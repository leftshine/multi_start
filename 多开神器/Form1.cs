﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace 多开神器
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void btn_open_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            //openFileDialog.Filter = "可执行文件(*.exe;*.lnk)|*.exe;*.lnk|所有文件|*.*";
            //openFileDialog.ValidateNames = true;
            //openFileDialog.CheckPathExists = true;
            //openFileDialog.CheckFileExists = true;

        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            tb_filepath.Text = openFileDialog.FileName;
            tb_filepath.Select(tb_filepath.Text.Length, 0);
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            int num = (int)nud_num.Value;
            string filepath = tb_filepath.Text;
            startProgram(num, filepath);
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string path = ((System.Array)e.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
            tb_filepath.Text = path;
            tb_filepath.Select(tb_filepath.Text.Length, 0);
            if (cb_quickMode.Checked)
            {
                int num = (int)nud_num.Value;
                string filepath = tb_filepath.Text;
                startProgram(num,filepath);
            }
        }

        private void startProgram(int num,string path)
        {
            
            for (int i = 0; i < num; i++)
            {
                try
                {
                    Process.Start(path);
                }catch(Exception)
                {
                    MessageBox.Show(" 无法启动程序，请检查程序路径是否正确" ,"启动错误");
                    break;
                }
                
            }
        }

        private void linkLabel_homepage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitee.com/leftshine/multi_start");
        }

        private void linkLabel_email_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("mailto:leftshine@foxmail.com");
        }
    }
}
