﻿namespace 多开神器
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tb_filepath = new System.Windows.Forms.TextBox();
            this.btn_open = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.nud_num = new System.Windows.Forms.NumericUpDown();
            this.lab_tip1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_quickMode = new System.Windows.Forms.CheckBox();
            this.linkLabel_homepage = new System.Windows.Forms.LinkLabel();
            this.toolTip_homepage = new System.Windows.Forms.ToolTip(this.components);
            this.linkLabel_email = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.nud_num)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "程序路径：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "可执行文件(*.exe;*.bat;*.cmd;*.com;*.lnk)|*.exe;*.bat;*.cmd;*.com;*.lnk|所有文件|*.*";
            this.openFileDialog.InitialDirectory = "C:";
            this.openFileDialog.ShowReadOnly = true;
            this.openFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_FileOk);
            // 
            // tb_filepath
            // 
            this.tb_filepath.Location = new System.Drawing.Point(79, 42);
            this.tb_filepath.Name = "tb_filepath";
            this.tb_filepath.Size = new System.Drawing.Size(217, 21);
            this.tb_filepath.TabIndex = 1;
            // 
            // btn_open
            // 
            this.btn_open.Location = new System.Drawing.Point(299, 40);
            this.btn_open.Name = "btn_open";
            this.btn_open.Size = new System.Drawing.Size(36, 23);
            this.btn_open.TabIndex = 2;
            this.btn_open.Text = "...";
            this.toolTip_homepage.SetToolTip(this.btn_open, "浏览");
            this.btn_open.UseVisualStyleBackColor = true;
            this.btn_open.Click += new System.EventHandler(this.btn_open_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "多开数量：";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(260, 76);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 23);
            this.btn_start.TabIndex = 5;
            this.btn_start.Text = "多开";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // nud_num
            // 
            this.nud_num.Location = new System.Drawing.Point(79, 79);
            this.nud_num.Name = "nud_num";
            this.nud_num.Size = new System.Drawing.Size(120, 21);
            this.nud_num.TabIndex = 6;
            this.nud_num.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // lab_tip1
            // 
            this.lab_tip1.AutoSize = true;
            this.lab_tip1.Location = new System.Drawing.Point(8, 13);
            this.lab_tip1.Name = "lab_tip1";
            this.lab_tip1.Size = new System.Drawing.Size(185, 12);
            this.lab_tip1.TabIndex = 7;
            this.lab_tip1.Text = "可以直接将想要多开的程序拖进来";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(8, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(329, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "提示:如不能正常多开,可尝试先结束掉多开程序的进程再多开";
            // 
            // cb_quickMode
            // 
            this.cb_quickMode.AutoSize = true;
            this.cb_quickMode.Location = new System.Drawing.Point(232, 11);
            this.cb_quickMode.Name = "cb_quickMode";
            this.cb_quickMode.Size = new System.Drawing.Size(108, 16);
            this.cb_quickMode.TabIndex = 9;
            this.cb_quickMode.Text = "拖入后立即启动";
            this.toolTip_homepage.SetToolTip(this.cb_quickMode, "勾选此项，将在拖入程序之后立即根据设置的多开数量启动程序");
            this.cb_quickMode.UseVisualStyleBackColor = true;
            // 
            // linkLabel_homepage
            // 
            this.linkLabel_homepage.AutoSize = true;
            this.linkLabel_homepage.Location = new System.Drawing.Point(258, 135);
            this.linkLabel_homepage.Name = "linkLabel_homepage";
            this.linkLabel_homepage.Size = new System.Drawing.Size(77, 12);
            this.linkLabel_homepage.TabIndex = 10;
            this.linkLabel_homepage.TabStop = true;
            this.linkLabel_homepage.Text = "访问项目主页";
            this.toolTip_homepage.SetToolTip(this.linkLabel_homepage, "点击访问项目主页，查看详细介绍、获取最新版本");
            this.linkLabel_homepage.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_homepage_LinkClicked);
            // 
            // linkLabel_email
            // 
            this.linkLabel_email.AutoSize = true;
            this.linkLabel_email.Location = new System.Drawing.Point(8, 135);
            this.linkLabel_email.Name = "linkLabel_email";
            this.linkLabel_email.Size = new System.Drawing.Size(179, 12);
            this.linkLabel_email.TabIndex = 11;
            this.linkLabel_email.TabStop = true;
            this.linkLabel_email.Text = "E-mail：leftshine@foxmail.com";
            this.linkLabel_email.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_email_LinkClicked);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(352, 158);
            this.Controls.Add(this.linkLabel_email);
            this.Controls.Add(this.linkLabel_homepage);
            this.Controls.Add(this.cb_quickMode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lab_tip1);
            this.Controls.Add(this.nud_num);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_open);
            this.Controls.Add(this.tb_filepath);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "多开神器";
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
            ((System.ComponentModel.ISupportInitialize)(this.nud_num)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox tb_filepath;
        private System.Windows.Forms.Button btn_open;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.NumericUpDown nud_num;
        private System.Windows.Forms.Label lab_tip1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cb_quickMode;
        private System.Windows.Forms.LinkLabel linkLabel_homepage;
        private System.Windows.Forms.ToolTip toolTip_homepage;
        private System.Windows.Forms.LinkLabel linkLabel_email;
    }
}

